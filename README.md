** Aloha ** 

Online Food Booking System. A Spring Web MVC Application.

## What I Earned from Aloha

	1. Implementation of Spring Security using JSON Web Token (JWT).
	
	2. JDBC connectivity (Oracle 11g) and complex SQL quires (CRUD + joins).
	
	3. Create RESTful APIs and give a structured Response.
	
	4. Layerd Architecture in MVC (Controller, service, service implementation, repository, repository implementation).
