package com.aloha.Utils;

import java.util.Map;

import org.json.JSONObject;

public class BaseResponse {
	@Override
	public String toString() {
		return "BaseResponse [responseCode=" + responseCode + ", msg=" + msg + ", data=" + data + "]";
	}
	private int responseCode;
	private String msg;
	/*private Map<String,Object> Data;*/
	private Object data;
	
	
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String buildResponse(int responseCode,String msg,Object data) {
		JSONObject json = new JSONObject();
		json.put("responseCode", responseCode);
		json.put("msg",msg);
		json.put("data", data);
		return json.toString();
	}
	
}
