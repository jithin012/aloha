package com.aloha.order;

import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepo {

	public OrderModel orderItem(OrderModel order) throws Exception;
	
}
