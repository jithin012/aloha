package com.aloha.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {
	@Autowired
	private OrderRepo orderRepo;
	
	@Override
	public void orderItem(OrderRequestModel requestModel) throws Exception {
		OrderModel ordermodel = new OrderModel();		
		ordermodel.setUserId(requestModel.getUserId());
		ordermodel.setListItem(requestModel.getListItem());
		ordermodel.setStatus("order received" );
		ordermodel.setOrderOn(new java.util.Date());
		ordermodel.setUpdatedOn(new java.util.Date());
		try {
			ordermodel = orderRepo.orderItem(ordermodel);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			throw new Exception("Error Detece at Service Layer");
		}
	}
		
}
