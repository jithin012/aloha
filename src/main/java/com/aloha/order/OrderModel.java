package com.aloha.order;

import java.util.Date;

public class OrderModel {
	private int userId;
	private String listItem;
	private String status;
	private Date orderOn;
	private Date updatedOn;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getListItem() {
		return listItem;
	}
	public void setListItem(String listItem) {
		this.listItem = listItem;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getOrderOn() {
		return orderOn;
	}
	public void setOrderOn(Date orderOn) {
		this.orderOn = orderOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	
}
