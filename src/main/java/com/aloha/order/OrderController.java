package com.aloha.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aloha.Utils.BaseResponse;

@RestController
public class OrderController {
	@Autowired
	private OrderService orderService;
	
	@RequestMapping( value = "/user/newOrder" , method = RequestMethod.POST)
	public Object order(@RequestBody OrderRequestModel orderRequestModel ) {
		BaseResponse baseResponse = new BaseResponse();
		if(isNotEmpty(orderRequestModel.getUserId()) && isNotEmpty(orderRequestModel.getListItem()) ) {			
			try {
				orderService.orderItem(orderRequestModel);
				return baseResponse.buildResponse(100, "Order Success.", null);
			}
			catch(Exception e) {
				System.out.println(e.getMessage());
				return baseResponse.buildResponse(101, "Internal Problem", null);
			}
		}
		else {
			return baseResponse.buildResponse(102, "ParaMeter missing", null);
		}
	}
	
	public boolean isNotEmpty(String str) {
		return str != null && !(str.isEmpty());
	}
	public boolean isNotEmpty(Integer number) {
		return number != null;
	}
}
