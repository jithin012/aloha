package com.aloha.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class OrderRepoImpl implements OrderRepo{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
		
	@Override
	public OrderModel orderItem(OrderModel order) throws Exception {
		try {
			jdbcTemplate.update("insert into orderitem (userId, listItem, status, orderOn, updatedOn) values (?, ?, ?, ?, ?)", order.getUserId(), order.getListItem(), order.getStatus(), order.getOrderOn(), order.getUpdatedOn());
			return order;
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			throw new Exception("Unable to add Order into the table");
		}
	}

}
