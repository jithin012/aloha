package com.aloha.order;

public interface OrderService {
	public void orderItem(OrderRequestModel requestModel) throws Exception;
}
