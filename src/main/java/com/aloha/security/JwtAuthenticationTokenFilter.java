package com.aloha.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.stereotype.Component;

public class JwtAuthenticationTokenFilter extends AbstractAuthenticationProcessingFilter{
	
	//https://stackoverflow.com/questions/46899568/use-abstractauthenticationprocessingfilter-for-multiple-urls
	public JwtAuthenticationTokenFilter() {
		super("/user/**");
		
	}
	
	@Override 
	public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse ) throws IOException,ServletException{
		
		String header = httpServletRequest.getHeader("Authorisation");
		if(header == null || !header.startsWith("Token ")) {
			throw new RuntimeException("Jwt TOKEN is missing ");
		}
		String authenticationToken = header.substring(6);
		JwtAuthenticationToken token = new JwtAuthenticationToken(authenticationToken);
		return getAuthenticationManager().authenticate(token);
	}
	
	 @Override
	    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
	        super.successfulAuthentication(request, response, chain, authResult);
	        chain.doFilter(request, response);
	    }
	

}