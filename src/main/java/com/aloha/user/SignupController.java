package com.aloha.user;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aloha.Utils.BaseResponse;

@RestController
public class SignupController {
	@Autowired(required=true)
	private SingupService signupService;
	
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public  Object signup( @RequestBody SignupRequestModel requestModel) throws Exception {
		String mobileNo =   requestModel.mobileNo;
		String password =   requestModel.password;
		String role     =   requestModel.role;
		String email	=   requestModel.email;
		String userName =   requestModel.userName;
		String address  =   requestModel.address;
		
		BaseResponse baseResponse = new BaseResponse();
		if(isNotEmpty(mobileNo) && isNotEmpty(password) && isNotEmpty(role) && isNotEmpty(email) && isNotEmpty(userName) && isNotEmpty(address)) {
			try {
				String token = signupService.addUser(requestModel);
				if(isNotEmpty(token)) {
					JSONObject data = new JSONObject();
					data.put("token", token);
					return baseResponse.buildResponse(100, "User registered successfully", data);
				}
				else {
					return baseResponse.buildResponse(101, "Internal Problem", null);
				}
			}
			catch(Exception e) {
				return baseResponse.buildResponse(101, e.getMessage(), null);
			}
		}
		else {
			return baseResponse.buildResponse(102, "ParaMeter missing", null);			
		}
}
	
	public boolean isNotEmpty(String str) {
		return str != null && !(str.isEmpty());
	}
	public boolean isNotEmpty(Integer number) {
		return number != null;
	}
}
