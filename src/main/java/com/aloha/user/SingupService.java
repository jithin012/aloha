package com.aloha.user;

import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public interface SingupService {
	public String addUser(SignupRequestModel signupModel) throws Exception;
}
