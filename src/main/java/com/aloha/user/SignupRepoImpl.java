package com.aloha.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SignupRepoImpl implements SignupRepo{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	//@Override
	 public Appuser addUser(Appuser appuser) throws Exception {
		try {
			jdbcTemplate.update("insert into appuser (mobileNo, role, email, address, createdOn, updatedOn) values (?, ?, ?, ?, ?, ?)",appuser.getMobileNo(), appuser.getRole(), appuser.getEmail(), appuser.getAddress(), appuser.getCreatedOn(), appuser.getUpdatedOn());
			return appuser;
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			throw new Exception("Unable to add User into the table");
		}
	 }
}
