package com.aloha.user;

import org.springframework.stereotype.Repository;

@Repository
public interface SignupRepo {

	public Appuser addUser(Appuser appuser) throws Exception;
}
