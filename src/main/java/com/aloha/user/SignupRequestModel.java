package com.aloha.user;

public class SignupRequestModel {
	
	String mobileNo;
	String password;
	String role;
	String email;
	String userName;
	String address;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getOtp() {
		return email;
	}
	public void setOtp(String email) {
		this.email = email;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Override
	public String toString() {
		return "SignupModel [mobileNo=" + mobileNo + ", password=" + password + ", role=" + role + ", email=" + email
				+ ", userName=" + userName + ", address=" + address + "]";
	}
	
}
