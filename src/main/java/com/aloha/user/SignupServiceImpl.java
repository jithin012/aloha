package com.aloha.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aloha.security.JwtTokenGenerator;
import com.aloha.security.JwtUser;

@Service
public class SignupServiceImpl implements SingupService{
	
	@Autowired
	private SignupRepo signupRepo;
	@Autowired
	private JwtTokenGenerator tokenGenerator;
	
	@Override
	public String addUser(SignupRequestModel requestModel) throws Exception {
		
		Appuser appuser = new Appuser();
		appuser.setMobileNo(requestModel.getMobileNo());
		appuser.setRole(requestModel.getRole());
		appuser.setEmail(requestModel.getEmail());
		appuser.setAddress(requestModel.getAddress());
		appuser.setCreatedOn(new java.util.Date());
		appuser.setUpdatedOn(new java.util.Date());	
		
		String token = "";
		try {
			 signupRepo.addUser(appuser);
			 JwtUser jwtUser =  new JwtUser();
			 jwtUser.setRole(requestModel.getRole());
			 jwtUser.setUserName(requestModel.getEmail());
			 jwtUser.setId(1234L);
			 token = tokenGenerator.generate(jwtUser);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			throw new Exception("Repository Level Error");
		}
		return token;
	}
}
