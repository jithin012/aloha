package com.aloha.user;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Appuser {
	
	@Id
	private int userId;
	private String mobileNo;
	private String role;
	private String email;
	private String address;	
	private Date createdOn;
	private Date updatedOn;

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
		
	@Override
	public String toString() {
		return "Appuser [userId=" + userId + ", mobileNo=" + mobileNo + ", role=" + role + ", email=" + email
				+ ", address=" + address + ", createdOn=" + createdOn + ", updatedOn=" + updatedOn + "]";
	}
}
